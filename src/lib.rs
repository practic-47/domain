use serde::{Serialize, Deserialize, de::DeserializeOwned};

    #[derive(PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
pub struct Link<T: Identifiable> {
    id: <T as Identifiable>::Id,
}

impl<T: Identifiable> Link<T> {
    pub fn new(id: <T as Identifiable>::Id) -> Self {
        Link {
            id
        }
    }
}

pub trait Identifiable: Ord + Serialize {
    type Id: Ord + Serialize + DeserializeOwned;

    fn identity() -> &'static str;

    fn id(&self) -> Self::Id;
}

pub mod courses {
    use crate::{Link, users::{Owner, OwnerId}, finances::{Price, Currency}, Identifiable};
    use std::collections::BTreeSet;
    use serde::{Serialize, Deserialize};

    #[derive(PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
    pub struct Course {
        id: u32,
        title: String,
        owner: Link<Owner>,
        price: Price,
        items: BTreeSet<Item>,
    }

    #[derive(PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
    pub struct Item {
        order: u32,
        title: String,
    }

    impl Course {
        pub fn new(title: String) -> Self {
            Course {
                id: 0,
                title,
                //TODO: extract owner id to arguments.
                owner: Link::new(OwnerId(1)),
                price: Price::new(10, Currency::EUR),
                items: BTreeSet::new(),
            }
        }
    }

    impl Identifiable for Course {
        type Id = u32;

        fn identity() -> &'static str {
            "courses"
        }

        fn id(&self) -> u32 {
            self.id.clone()
        }
    }
}

pub mod finances {
    use serde::{Serialize, Deserialize};

    #[derive(PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
    pub struct Price {
        value: u64,
        currency: Currency,
    }

    #[derive(PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
    pub enum Currency {
        RUB,
        XOR,
        USD,
        EUR,
    }

    impl Price {
        pub fn new(value: u64, currency: Currency) -> Self {
            Price {
                value, currency
            }
        }
    }
}

pub mod users {
    use serde::{Serialize, Deserialize};
    use super::Identifiable;
    use std::{str::FromStr, num::ParseIntError };

    #[derive(PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
    pub struct User {
        id: UserId,
        name: String,
    }

    #[derive(PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
    pub struct Owner {
        id: OwnerId,
        name: String,
    }

    #[derive(Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
    pub struct UserId(pub u32);
    #[derive(Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
    pub struct OwnerId(pub u32);

    impl User {
        pub fn new(name: &str) -> Self {
            User {
                id: UserId(0),
                name: name.to_string()
            }
        }
    }

    impl Identifiable for User {
        type Id = UserId;

        fn identity() -> &'static str {
            "users"
        }

        fn id(&self) -> UserId {
            self.id.clone()
        }
    }

    impl Identifiable for Owner {
        type Id = OwnerId;

        fn identity() -> &'static str {
            "owners"
        }

        fn id(&self) -> OwnerId {
            self.id.clone()
        }
    }

    impl FromStr for UserId {
         type Err = ParseIntError;

        fn from_str(s: &str) -> Result<Self, Self::Err> {
            Ok(
                UserId(s.parse()?)
            )
        }
    }
}

pub mod orders {
    use crate::Identifiable;
    use serde::{Serialize, Deserialize};

    #[derive(PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
    pub struct Order {
        id: u32,
    }

    impl Order {
        pub fn new() -> Self {
            Order {
                id: 0
            }
        }
    }

    impl Identifiable for Order {
        type Id = u32;

        fn identity() -> &'static str {
            "orders"
        }

        fn id(&self) -> u32 {
            self.id
        }
    }
}
